## TYPE 
crear type en controller/types/index.ts

```typescript
/**
 * Auth JSON response for Controllers
 */
export type AuthResponse = {
    message: string,
    token: string
}
```

