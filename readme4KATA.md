creamos la entidad kata con su interface
domain/interfaces/IKata.interface.ts
```typescript

export enum KataLevel {
    BASIC = 'Basic',  // SI NO SE PONE VALOR, TOMAR 0 , 1 , 2
    MEDIUM = 'Medium',
     HIGH = 'High'
}

export interface IKata {

    name: string,
    description: string,
    level: KataLevel,
    intents: number,
    stars: number,
    creator: string, //id of user
    solution: string,
    participants: string[]
}
```

entidad IKata

``` typescript

import mongoose from "mongoose";

import { IKata } from "../interfaces/IKata.interface";

export const KataEntity = () => {

    let kataSchema = new mongoose.Schema<IKata>(
        {
            name: { type: String, required: true},
            description: { type: String, required: true},
            level:{ type: String, required: true}, // Debes poder filtrar las Katas disponibles por nivel de dificultad
            intents: { type: Number, required: true},
            stars: { type: Number, required: true},
            creator: { type: String, required: true},
            solution: { type: String, required: true},
            participants: { type: [], required: true }
        }
    )
    return mongoose.models.Kata || mongoose.model<IKata>('Kata', kataSchema);
}
```

** ORM

src/domain/orm/Kata.orm.ts


```typescript

import {  KataEntity } from "../entities/Kata.Entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { response } from "express";
import { IKata } from "../interfaces/IKata.interface";

// Environment variables
import dotenv from 'dotenv';

// Configuration of environment variables

// CRUD
/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */
export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {

    try {
        let kataModel = KataEntity();

        let response: any = {};

        // search all katas (using pagination)
        await kataModel.find()
        .limit(limit)
        .skip((page - 1) * limit)
        .exec().then((katas: IKata[]) => {
            response.katas = katas;
        });

        // Count total documents in collection "Katas"
        await kataModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        })

        return await response;

    } catch (error) {
        LogError(`[ORM ERROR]: Getting all Katas: ${error}`)
    }
}

// - Get Kata By ID.
export const getKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();

        // Search kata by ID
        return await kataModel.findById(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Getting kata error by ID: ${error}`)
    }
}

export const deleteKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();
        // Delete Kata By ID.
        return await kataModel.findByIdAndDelete({ _id: id});
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting Kata error By ID: ${error}`)
    }
}

export const createKata =async (kata: IKata): Promise<any | undefined> => {
    
    try {
        let kataModel = KataEntity();

        //Obviamente faltan comprobaciones.

        return await kataModel.create(kata);

    } catch (error) {
        LogError(`[ORM ERROR]:Creating Katta: ${error}`)
    }
}

// Update Kata By ID
export const updateKataByID = async (id: string, kata: IKata): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();

        // Update Kata.
        return await kataModel.findByIdAndUpdate(id, kata);
    } catch (error) {
        LogError(`[ORM ERROR]:Updating Katta: ${error}`);
    }
}import {  KataEntity } from "../entities/Kata.Entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { response } from "express";
import { IKata } from "../interfaces/IKata.interface";

// Environment variables
import dotenv from 'dotenv';

// Configuration of environment variables

// CRUD
/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */
export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {

    try {
        let kataModel = KataEntity();

        let response: any = {};

        // search all katas (using pagination)
        await kataModel.find()
        .limit(limit)
        .skip((page - 1) * limit)
        .exec().then((katas: IKata[]) => {
            response.katas = katas;
        });

        // Count total documents in collection "Katas"
        await kataModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        })

        return await response;

    } catch (error) {
        LogError(`[ORM ERROR]: Getting all Katas: ${error}`)
    }
}

// - Get Kata By ID.
export const getKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();

        // Search kata by ID
        return await kataModel.findById(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Getting kata error by ID: ${error}`)
    }
}

export const deleteKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();
        // Delete Kata By ID.
        return await kataModel.findByIdAndDelete({ _id: id});
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting Kata error By ID: ${error}`)
    }
}

export const createKata =async (kata: IKata): Promise<any | undefined> => {
    
    try {
        let kataModel = KataEntity();

        //Obviamente faltan comprobaciones.

        return await kataModel.create(kata);

    } catch (error) {
        LogError(`[ORM ERROR]:Creating Katta: ${error}`)
    }
}

// Update Kata By ID
export const updateKataByID = async (id: string, kata: IKata): Promise<any | undefined> => {

    try {
        let kataModel = KataEntity();

        // Update Kata.
        return await kataModel.findByIdAndUpdate(id, kata);
    } catch (error) {
        LogError(`[ORM ERROR]:Updating Katta: ${error}`);
    }
}

```

Paso siguiente controller/interfaces/index.ts

```typescript
export interface IKatasController {

    // Read all katas from database or dind katas by ID ( ObjetctID)
    getKatas(page: number, limit: number, id?:string): Promise<any>
    // Delete Kata by ID
    deleteKata(id: string): Promise<any>
    // Create new Kata
    createKata(kata: IKata): Promise<any>
    // Update Kata
    updateKata(id: string, kata: IKata): Promise<any>
}

```

src/controller/

```typescript
import { Get, Delete, Post, Put, Query, Route, Tags} from "tsoa";
import { IKatasController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { BasicResponse } from "./types";
import { createKata, getAllKatas, getKataByID, deleteKataByID, updateKataByID } from "../domain/orm/Kata.orm";
import { IKata } from "../domain/interfaces/IKata.interface";

// ORM - Katas

@Route("/api/katas")
@Tags("KatasController")
export class KatasController implements IKatasController {
    
 /**
  * Endpoint to retreive the katas in the Collection "Katas" of DB
  * @param page 
  * @param limit 
  * @param id Id of Kata to retreive (optional)
  * @returns  All katas o kata found by ID
  */
    @Get("/")
    public async getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
       
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] Get Katas By ID: ${id}`);

            response = await getKataByID(id);
        } else {
            LogSuccess(`[api/katas] Get All Katas`);
            response = await getAllKatas(page, limit);
        }

        return response;
        
    }

    /**
     * Endpoint to delete the Katas in the Collection "Katas" of DB
     * @param {string} id Id of kata to delete (optional)
     * @returns message informing if deletion was corrects
     */
    @Delete("/")
    public async deleteKata(@Query()id?: string): Promise<any> {
        
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] delete Kata By ID: ${id}`);

            // Delete Kata By ID.
            await deleteKataByID(id).then((r) => {
                response = {
                    message: `Kata with id ${id} deleted successfuly`
                }
            });
        } else {
            LogWarning('[/api/katas] Delete Kata Request WITHOUT ID');
            response = {
                message: 'Please, provide an ID to remove from database'
            }
          
        }
       return response;
    }

    @Post("/")
    public async createKata(kata: IKata): Promise<any> {
      
        let response: any = '';
        if(kata){
        await createKata(kata).then((r) => {
            LogSuccess(`[api/katas] create Katas`);
            response = {
                message: `Kata created successfully: ${kata.name}`
            }
        });
        } else {
            LogWarning('[api/katas] register needs Kata Entity');
            response = {
                message: 'Kata not Registered: Please, provide a Kata Entity to create one'
            }
        }
         
       return response;
    }

    @Put("/")
    public async updateKata(id: string, kata: IKata): Promise<any> {
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] update kata By ID: ${id} `);

            await updateKataByID(id, kata).then((r) => {
                response = {
                    message: `kata with id ${id} updated successfuly`
                }
            })
        } else {
            LogWarning('[/api/katas] Update kata Request WITHOUT ID');
            response = {
                message: 'Please, provide an ID to update an existing kata'
            }
        }
        return response;
    }

}
```

## Ahora vamos a crear KataRouter
(src/routes/KataRouter.ts)

```typescript
import express, { Request, Response } from "express";
import { KatasController } from "../controller/KatasController";
import { LogInfo } from "../utils/logger";
import { userEntity } from "../domain/entities/User.entity";

// BodyParser from BODY from requests.
import bodyParser from "body-parser";

let jsonParser = bodyParser.json();

// JWT Verifier Middleware
import { verifyToken } from "../middlewares/verifyToken.middleware";
import { IKata, KataLevel } from "../domain/interfaces/IKata.interface";

// Router from express
let kataRouter = express.Router();


kataRouter.route('/')
    .get(verifyToken, async (req: Request, res: Response) => {

       // Obtain a Query Param ID
       let id: any = req?.query?.id;

       // Pagination
       let page: any = req?.query?.page || 1;
       let limit: any = req?.query?.limit || 10;

       LogInfo(`Query Param: ${id}`)

        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);

        return res.status(200).send(response);
    })
    .post(jsonParser, verifyToken, async (req: Request, res: Response) => {
    
        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || '';
        let level: KataLevel = req?.body?.KataLevel || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || '';
        let participants: string[] = req?.body?.participants || [];

        if(name && description && level && intents && stars && creator && solution && participants){

            const controller: KatasController = new KatasController();


            let kata: IKata = {
                 name,
                 description,
                 level,
                 intents,
                 stars,
                 creator,
                 solution,
                 participants
            }
             
     
             // Obtain Response
             const response: any = await controller.createKata(kata);
     
             return res.status(201).send(response);

        }else {
            return res.status(400).send({
                message: '[ERROR] Creating Kata. you need to send all attrs of kata to update it'
            })
        }
       
    
    })
    .delete(async (req: Request, res: Response) => {
        // Obtain a Query param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param in method Delete: ${id}`);

        // Controller instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.deleteKata(id);

        return res.send(response);
    })
    .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;
    
        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || 'Default description';
        let level: KataLevel = req?.body?.KataLevel || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || 'Default Solution';
        let participants: string[] = req?.body?.participants || ['a'];

        if(name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0 ){

            const controller: KatasController = new KatasController();


            let kata: IKata = {
                 name,
                 description,
                 level,
                 intents,
                 stars,
                 creator,
                 solution,
                 participants
            }
             
     
             // Obtain Response
             const response: any = await controller.updateKata(id, kata);
     
             return res.status(200).send(response);

        }else {
            return res.status(400).send({
                message: '[ERROR] Updating Kata. you need to send all attrs of kata to update it'
            })
        }
       
    });


export default kataRouter;
```

y en src/server/index.ts agregarlo donde corresponda

```typescript

server.use('/katas', kataRouter) // http://localhost:8000/api/katas --> KataRouter

```

## PARTE 2

# Encontrar las katas de un usuario

en la interfaz agregamos

```typescript
 // Get Katas of User
    getKatas(id?:string): Promise<any>
```

# Ahora implementaremos en UsersController



