import express,{ Request, Response } from "express";

import { UserController} from "../controller/UsersController"
import { LogInfo } from "../utils/logger";

// Bcrypt for password
import bcrypt from 'bcrypt';
import { IUser } from "../domain/interfaces/IUser.interface";

// Router from express
let userRouter = express.Router();

// Body parser to read BODY from requests
import bodyParser from "body-parser";
// JWT verifier Middleware
import { verifyToken } from "../middlewares/verifyToken.middleware";

let jsonParser = bodyParser.json(); // lo usaremos como middleware

// GET http://localhost:8000/api/users?id=6415df526b3caca0a78acdcb
userRouter.route('/')
    .get(verifyToken, async (req: Request, res: Response) => {
    
        // Refactorización: Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`)


        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        // Controller Instance to execute method
        const controller: UserController = new UserController();
        // Obtain Response
        const response: any = await controller.getUsers(page, limit, id); //modificamos agregando id
        // Send to the client the response
        return res.status(200).send(response);

    })
    .delete(verifyToken,async (req: Request, res: Response) => {
        // Obtain a Query Param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param in method Delete: ${id}`)
        // Controller Instance to execute method
        const controller: UserController = new UserController();
        // Obtain Response
        const response: any = await controller.deleteUsers(id); //modificamos agregando id
        // Send to the client the response
        return res.status(response.status).send(response);


    })
    .put(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;
        

        const controller: UserController = new UserController();

        let name: any = req?.query?.name;
        let age: any = req?.query?.age;
        let email: any = req?.query?.email;
        LogInfo(`Query Param: ${id}, ${name}, ${age}, ${email}`)
        let user = {
            name: name,
            email: email,
            age: age
        }

        // Obtain Response
        const response: any = await controller.updateUser(id, user);

        return res.status(response.status).send(response.message);
    }); //también se puese continuar con .post o .delete para borrar algo
// AGREGANDO NUEVA SUBRATA
// http://localhost:8000/api/users/katas
userRouter.route('/katas')
    .get(verifyToken, async (req: Request, res: Response) => {

        // Obtaind a Query Param (ID)
        let id: any = req?.query?.id;

        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        // Controller Instance to execute method
        const controller: UserController = new UserController();

        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);

        return res.status(200).send(response);
    })

// Export Users Router
export default userRouter;

/**
 * Get Document => 200 OK
 * Creation Documents => 201 OK
 * Deletion of Documents => 200 (entity) / 204 (No return)
 * Update of Documents => 200 (entity) / 204 (No return)
 */