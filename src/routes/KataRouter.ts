import express, { Request, Response } from "express";
import { KatasController } from "../controller/KatasController";
import { LogInfo } from "../utils/logger";
import { userEntity } from "../domain/entities/User.entity";
// BodyParser from BODY from requests.
import bodyParser from "body-parser";

let jsonParser = bodyParser.json();

// JWT Verifier Middleware
import { verifyToken } from "../middlewares/verifyToken.middleware";
import { IKata, KataLevel } from "../domain/interfaces/IKata.interface";

// Upload file
import {  UploadController } from '../controller/UploadController';
import { uploadMiddleware } from "../middlewares/multerMiddleware";

// Router from express
let kataRouter = express.Router();




// http://localhost:8000/api/katas
kataRouter.route('/')
    .get(verifyToken, async (req: Request, res: Response) => {

        // Obtain a Query Param ID
        let id: any = req?.query?.id;

        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        LogInfo(`Query Param: ${id}`)

        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);

        return res.status(200).send(response);
    })
    .post(jsonParser, verifyToken, async (req: Request, res: Response) => {

        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || '';
        let level: KataLevel = req?.body?.KataLevel || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || '';
        let participants: string[] = req?.body?.participants || [];

        if (name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0) {

            const controller: KatasController = new KatasController();


            let kata: IKata = {
                name,
                description,
                level,
                intents,
                stars,
                creator,
                solution,
                participants
            }


            // Obtain Response
            const response: any = await controller.createKata(kata);

            return res.status(201).send(response);

        } else {
            return res.status(400).send({
                message: '[ERROR] Creating Kata. you need to send all attrs of kata to update it'
            })
        }


    })
    .delete(async (req: Request, res: Response) => {
        // Obtain a Query param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param in method Delete: ${id}`);

        // Controller instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.deleteKata(id);

        return res.send(response);
    })
    .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;

        // Read from body
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || '';
        let level: KataLevel = req?.body?.KataLevel || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || '';
        let participants: string[] = req?.body?.participants || [];

        if (name && description && level && intents >= 0 && stars >= 0 && creator && solution && participants.length >= 0) {

            const controller: KatasController = new KatasController();


            let kata: IKata = {
                name,
                description,
                level,
                intents,
                stars,
                creator,
                solution,
                participants
            }


            // Obtain Response
            const response: any = await controller.updateKata(id, kata);

            return res.status(200).send(response);

        } else {
            return res.status(400).send({
                message: '[ERROR] Updating Kata. you need to send all attrs of kata to update it'
            })
        }


    });

    kataRouter.route('/uploadFile')
  .post(uploadMiddleware, async (req: any, res: any) => {
    console.log("hola")
    const controller: UploadController = new UploadController();
    const response: any = await controller.uploadFile(req, res);
    res.json(response);
  });






export default kataRouter;