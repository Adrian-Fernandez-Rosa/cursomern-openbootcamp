import {  kataEntity } from "../entities/Kata.Entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { response } from "express";
import { IKata } from "../interfaces/IKata.interface";

// Environment variables
import dotenv from 'dotenv';

// Configuration of environment variables

// CRUD
/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */
export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {

    try {
        let kataModel = kataEntity();

        let response: any = {};

        // search all katas (using pagination)
        await kataModel.find()
        .limit(limit)
        .skip((page - 1) * limit)
        .exec().then((katas: IKata[]) => {
            response.katas = katas;
        });

        // Count total documents in collection "Katas"
        await kataModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        })

        return await response;

    } catch (error) {
        LogError(`[ORM ERROR]: Getting all Katas: ${error}`)
    }
}

// - Get Kata By ID.
export const getKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Search kata by ID
        return await kataModel.findById(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Getting kata error by ID: ${error}`)
    }
}

export const deleteKataByID = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Delete Kata By ID.
        const result = await kataModel.findByIdAndDelete({ _id: id});

        if(result == null)
        throw new Error(`Kata with id ${id} not found`)
        
        return await result;
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting Kata error By ID: ${error}`)
    }
}

export const createKata =async (kata: IKata): Promise<any | undefined> => {
    
    try {
        let kataModel = kataEntity();

        //Obviamente faltan comprobaciones.

        return await kataModel.create(kata);

    } catch (error) {
        LogError(`[ORM ERROR]:Creating Katta: ${error}`)
    }
}

// Update Kata By ID
export const updateKataByID = async (id: string, kata: IKata): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Update Kata.
        return await kataModel.findByIdAndUpdate(id, kata);
    } catch (error) {
        LogError(`[ORM ERROR]:Updating Katta: ${error}`);
    }
}