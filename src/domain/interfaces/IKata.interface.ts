export enum KataLevel {
    BASIC = 'Basic',  // SI NO SE PONE VALOR, TOMAR 0 , 1 , 2
    MEDIUM = 'Medium',
     HIGH = 'High'
}

export interface IKata {

    name: string,
    description: string,
    level: KataLevel,
    intents: number,
    stars: number,
    creator: string, //id of user
    solution: string,
    participants: string[]
}