import { Get, Delete, Post, Put, Query, Route, Tags} from "tsoa";
import { IKatasController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { BasicResponse } from "./types";
import { createKata, getAllKatas, getKataByID, deleteKataByID, updateKataByID } from "../domain/orm/Kata.orm";
import { IKata } from "../domain/interfaces/IKata.interface";

// ORM - Katas

@Route("/api/katas")
@Tags("KatasController")
export class KatasController implements IKatasController {
    
 /**
  * Endpoint to retreive the katas in the Collection "Katas" of DB
  * @param page 
  * @param limit 
  * @param id Id of Kata to retreive (optional)
  * @returns  All katas o kata found by ID
  */
    @Get("/")
    public async getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
       
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] Get Katas By ID: ${id}`);

            response = await getKataByID(id);
        } else {
            LogSuccess(`[api/katas] Get All Katas`);
            response = await getAllKatas(page, limit);
        }

        return response;
        
    }

    /**
     * Endpoint to delete the Katas in the Collection "Katas" of DB
     * @param {string} id Id of kata to delete (optional)
     * @returns message informing if deletion was corrects
     */
    @Delete("/")
    public async deleteKata(@Query()id?: string): Promise<any> {
        
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] delete Kata By ID: ${id}`);
           
            const deletedKata = await deleteKataByID(id);
            // Delete Kata By ID.
          if(!deletedKata)
          {
            response = {
                message: `Kata with id ${id} not found`
            }
          }else {
            response = {
                message: `Kata with id ${id} deleted successfuly`
            }
          }
        } else {
            LogWarning('[/api/katas] Delete Kata Request WITHOUT ID');
            response = {
                message: 'Please, provide an ID to remove from database'
            }
          
        }
       return response;
    }

    @Post("/")
    public async createKata(kata: IKata): Promise<any> {
      
        let response: any = '';
        if(kata){
        await createKata(kata).then((r) => {
            LogSuccess(`[api/katas] create Katas`);
            response = {
                message: `Kata created successfully: ${kata.name}`
            }
        });
        } else {
            LogWarning('[api/katas] register needs Kata Entity');
            response = {
                message: 'Kata not Registered: Please, provide a Kata Entity to create one'
            }
        }
         
       return response;
    }

    @Put("/")
    public async updateKata(id: string, kata: IKata): Promise<any> {
        let response: any = '';

        if(id){
            LogSuccess(`[/api/katas] update kata By ID: ${id} `);

            await updateKataByID(id, kata).then((r) => {
                response = {
                    message: `kata with id ${id} updated successfuly`
                }
            })
        } else {
            LogWarning('[/api/katas] Update kata Request WITHOUT ID');
            response = {
                message: 'Please, provide an ID to update an existing kata'
            }
        }
        return response;
    }

    // api/katas/upload
    @Post("/upload")
    public async updateKataFile(): Promise<any> {
        let response: any = '';

        console.log("hola estoy en el controler")
        
    }

}