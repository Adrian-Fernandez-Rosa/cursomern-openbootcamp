import express from 'express';
import { Controller, Post, Route } from 'tsoa';

interface MulterFile extends Express.Multer.File {}

@Route('katas')
export class UploadController extends Controller {

 
  @Post('uploadFile')
  async uploadFile(req: express.Request, res: express.Response): Promise<any> {
    const files: MulterFile[] = req.files as MulterFile[];
  console.log("HOla")
    if (files) {
      this.setStatus(201);
      let filePaths = files.map(file => ({ path: file.path }));
      return { success: true, files: filePaths };
    } else {
      this.setStatus(500);
      return { success: false, error: "Files not uploaded correctly" };
    }
  }
  

}