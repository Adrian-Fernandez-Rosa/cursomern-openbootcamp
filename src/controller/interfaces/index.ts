import { Controller } from "tsoa";
import { IKata } from "../../domain/interfaces/IKata.interface";
import { IUser } from "../../domain/interfaces/IUser.interface";
import { BasicResponse, DateResponse } from "../types";


export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>; // Método para saludar
}

export interface IGoodbyeController {
    getMessage(name?:string, date?:Date): Promise<DateResponse>;
}

export interface IUserController {

    // Read all users from database or Find User by ID (ObjectID)
    getUsers(page: number, limit: number,id?: string): Promise<any>
    // Get Katas of User
    getKatas(page: number, limit: number, id?: string): Promise<any>
    // Delete User By ID
    deleteUsers(id: string): Promise<any>
    // Update user
    updateUser(id: string, user: any): Promise<any>
}

export interface IKatasController {

    // Read all katas from database or dind katas by ID ( ObjetctID)
    getKatas(page: number, limit: number, id?:string): Promise<any>
    // Delete Kata by ID
    deleteKata(id: string): Promise<any>
    // Create new Kata
    createKata(kata: IKata): Promise<any>
    // Update Kata
    updateKata(id: string, kata: IKata): Promise<any>
    // Uploade File to kata
    updateKataFile(): Promise<any> 
}


export interface IAuthController {
    // Register users
    registerUser(user: IUser): Promise<any>
    // Login user
    loginUser(auth: any): Promise<any>
}


